main: main.cpp
	g++ -o main main.cpp

main2: main2.o Clock.o
	g++ -o main2 main2.o Clock.o

main2.o: main2.cpp Clock.h
	g++ -c main2.cpp 

Clock.o: Clock.cpp Clock.h
	g++ -c Clock.cpp

clear:
	rm main main2 *.o 
